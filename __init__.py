from flask import Flask,render_template,flash,request
from wtforms import Form, TextField, FileField, validators
from werkzeug.utils import secure_filename
import os
import imageditor

DEBUG=True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'


UPLOAD_FOLDER = '/tmp/app/'


class crosslineform(Form):
    xsize = TextField('Size of X:', validators=[validators.required()])
    ysize = TextField('Size of Y:', validators=[validators.required()])
    filename = FileField('File path:', validators=[validators.required()])


@app.route("/", methods=['GET', 'POST'])
def mainpage():
    form = crosslineform(request.form)

    print (form.errors)
    if request.method == 'POST':
        xsize = request.form['xsize']
        ysize = request.form['ysize']
        filename = request.files['filename']
        filename_path = secure_filename(filename.filename)
        filename.save(os.path.join('/tmp/app/', filename_path))
        imageed = imageditor.imageEditor()
        imageed.insertlines('/tmp/app/'+filename_path,int(xsize),int(ysize))
    return render_template('index.html',form=form)

if __name__ == '__main__':
    app.run()

