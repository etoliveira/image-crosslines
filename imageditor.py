import cv2 as cv

class imageEditor():
    def image(self,imagefilepath):
        imagefile = imagefilepath
        return self
    
    def insertlines(self,imagefile,xsize,ysize):
        img = cv.imread(imagefile)
        yaxis,xaxis,channels = img.shape
        pos_x = 0
        pos_y = 0
        while (pos_x<=xaxis):
            cv.line(img,(0,pos_x+ysize),(xaxis,pos_x+ysize),(0,255,0),1)
            cv.line(img,(pos_x+ysize,0),(pos_x+ysize,yaxis),(0,255,0),1)
            pos_x=pos_x+ysize
            pos_y=pos_y+xsize
        cv.imshow("Ploted Image", img)
        cv.waitKey(0)
        cv.destroyAllWindows()

